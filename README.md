## Использование:

- Склонировать репозиторий.
- Скопировать файлы из папки `user` в папку с SLX моделью.
- - Выставить для SLX модели дискретный решатель с фиксированым шагом.
- Скопировать ваш `mjkey.txt` в папку с SLX файлом.
- Создать файл с XML моделью робота.
- - Шар решателя Simulink должен совпадать с `timeStep` XML модели.
- Открыть SLX модель и вставить S-Function блок.
- Указать имя S-Функции как `MuJoCo`.
- Указать параметры S-Функции как `MuJoCoParse('model.xml')`, где `model.xml` - ваш файл с XML моделью робота.
- Желательно настроить скорость симуляции под ваши нужды.
- Запустить симуляцию модели.

### Использование MATLAB переменных в XML модели:

MATLAB:
```matlab
shinRadius = pi / 12;
shinLength = 0.2;
shinPosition = [0.1, 0.1, 0.3];
m1 = 8.72;
```

Ваш XML:
```xml
<geom name="shin" type="cylinder" size="shinRadius shinLength" pos="shinPosition" mass="m1" />
```

Результирующий XML:
```xml
<geom name="shin" type="cylinder" size="0.2618 0.2" pos="0.1 0.1 0.3" mass="8.72" />
```