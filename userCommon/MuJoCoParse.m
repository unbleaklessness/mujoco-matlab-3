function result = MuJoCoParse(xmlPath, libraryPath)

    dom = xmlread(xmlPath);
    
    [nInputs, nOutputs, positions0, velocities0, inputNames, outputNames, positionNames, velocityNames, variableNames] = MuJoCoParseInternal(dom, 0, 0, [], [], {}, {}, {}, {}, {});
    
    data = xmlwrite(dom);
    
    file = fopen('MuJoCoModel.xml', 'w+');
    fprintf(file, data);
    fclose(file);

    writeNames('MuJoCoNames.h', inputNames, outputNames, positionNames, velocityNames, variableNames);
    
    if isempty(libraryPath) 
        libraryPath = '.';
    end

    if ispc
        [status, message] = system('echo Enter | .\MuJoCoTest.exe MuJoCoModel.xml');
    elseif isunix
        [status, message] = system(strcat('echo | LD_LIBRARY_PATH=', libraryPath, ' ./MuJoCoTest.run MuJoCoModel.xml'));
    else
        error('Supported only Linux and Windows.');
    end
    
    if status ~= 0
        error(message);
    end
    
    result{1} = nInputs;
    result{2} = nOutputs;
    result{3} = positions0;
    result{4} = velocities0;
end

function [nInputs, nOutputs, positions0, velocities0, inputNames, outputNames, positionNames, velocityNames, variableNames] = MuJoCoParseInternal(node, nInputs, nOutputs, positions0, velocities0, inputNames, outputNames, positionNames, velocityNames, variableNames)

    children = node.getChildNodes();
    nChildren = children.getLength();
    
    supportedAttributes = {
        'pos', 'size', 'gravity', 'timestep', 'position0', ...
        'velocity0', 'mass', 'gear', 'diaginertia', 'armature', ...
        'limited', 'range', 'axis', 'rgba', 'euler', 'kv', 'kp', ...
        'frictionloss', 'condim', 'friction' ...
    };

    nameAttribute = 'name';

    function getValue0(child, attributeName, variant, nValues)
        value0 = [];
        value0Attribute = child.getAttribute(attributeName);
        if ~value0Attribute.isEmpty()
            value0String = convertCharsToStrings(value0Attribute.toCharArray());
            value0String = strtrim(value0String{1, 1});
            value0String = strrep(value0String, '  ', ' ');
            value0String = strrep(value0String, '   ', ' ');
            value0String = strrep(value0String, '    ', ' ');
            value0 = cellfun(@(x) str2double(x), split(value0String, ' '))';
            if any(isnan(value0))
                error('Could not read initial condition `%s`.', value0String);
            end
        end
        defaultValue0 = zeros(1, max(nValues, length(value0)));
        defaultValue0(1, 1:length(value0)) = value0;
        value0 = defaultValue0(1, 1:nValues);
        switch variant
            case 1
                positions0 = [positions0, value0];
            case 2
                velocities0 = [velocities0, value0];
            otherwise
                error('Unknown variant `%d` for `getValue0` function.', variant)
        end
        child.removeAttribute(attributeName);
    end

    function getValues0(child, nPositions0, nVelocities0)
        getValue0(child, 'position0', 1, nPositions0);
        getValue0(child, 'velocity0', 2, nVelocities0);
    end

    function getNames(child, nInputs, nOutputs, nPositions, nVelocities)
        thisNameAttribute = child.getAttribute(nameAttribute);
        if ~thisNameAttribute.isEmpty()
            nameString = convertCharsToStrings(thisNameAttribute.toCharArray());
            nameString = strtrim(nameString{1, 1});
            for index = 1:nInputs
                inputNames{end + 1} = formatName(index);
            end
            for index = 1:nOutputs
                outputNames{end + 1} = formatName(index);
            end
            for index = 1:nPositions
                positionNames{end + 1} = formatName(index);
            end
            for index = 1:nVelocities
                velocityNames{end + 1} = formatName(index);
            end
        else
            error('Node `%s` has no name.', convertCharsToStrings(child.getNodeName().toCharArray()));
        end
        function result = formatName(index)
            result = strcat(nameString, '_', num2str(index));
        end
    end
    
    for index1 = 0:nChildren - 1
        child = children.item(index1);
        if child.getNodeType ~= 1 % Handle only tag nodes.
            continue
        end
        for index2 = 1:length(supportedAttributes)
            attribute = child.getAttribute(supportedAttributes{index2});
            if attribute.isEmpty()
                continue
            end
            attributeValue = convertCharsToStrings(attribute.toCharArray());
            attributeValue = strtrim(attributeValue{1, 1});
            splitted = split(attributeValue, ' ');
            for index3 = 1:length(splitted)
                if ~isvarname(splitted{index3})
                    continue
                end
                try
                    evaluated = evalin('base', splitted{index3});
                catch
                    error('Template variable `%s` is invalid MATLAB variable.', splitted{index3});
                end
                if iscell(evaluated)
                    error('Template variable `%s` cannot be a cell array.', splitted{index3});
                end
                if islogical(evaluated)
                    if evaluated
                        evaluated = 'true';
                    else
                        evaluated = 'false';
                    end
                end
                if ~isvector(evaluated) || (~isreal(evaluated) && ~ischar(evaluated))
                    error('Template variable `%s` is not a vector of real numbers or characters.', splitted{index3});
                end
                evaluated = reshape(evaluated, 1, length(evaluated));
                variableNames{end + 1} = splitted{index3};
                if isreal(evaluated)
                    splitted{index3} = num2str(evaluated, '%.12f ');
                elseif ischar(evaluated)
                    splitted{index3} = evaluated;
                end
            end
            child.setAttribute(supportedAttributes{index2}, strjoin(splitted));
        end
        childName = convertCharsToStrings(child.getNodeName().toCharArray());
        if strcmp(childName, 'freejoint')
            n = 7; m = 6;
            getValues0(child, n, m);
            getNames(child, 0, 0, n, m);
        elseif strcmp(childName, 'joint')
            typeAttribute = child.getAttribute('type');
            if typeAttribute.isEmpty() % Hinge joint.
                n = 1; m = 1;
                getValues0(child, n, m);
                getNames(child, 0, 0, n, m);
            else
                typeValue = convertCharsToStrings(typeAttribute.toCharArray());
                typeValue = strtrim(typeValue{1, 1});
                switch typeValue
                    case 'ball'
                        n = 4; m = 3;
                        getValues0(child, n, m);
                        getNames(child, 0, 0, n, m);
                    case 'slide'
                        n = 1; m = 1;
                        getValues0(child, n, m);
                        getNames(child, 0, 0, n, m);
                    case 'hinge'
                        n = 1; m = 1;
                        getValues0(child, n, m);
                        getNames(child, 0, 0, n, m);
                    case 'free'
                        n = 7; m = 6;
                        getValues0(child, n, m);
                        getNames(child, 0, 0, n, m);
                    otherwise
                        error('Unknown joint type `%s`.', typeValue);
                end
            end
        elseif strcmp(childName, 'motor') || strcmp(childName, 'velocity') || ...
                strcmp(childName, 'position') || strcmp(childName, 'general') || ...
                strcmp(childName, 'cylinder') || strcmp(childName, 'muscle')
            n = 1;
            nInputs = nInputs + n;
            getNames(child, n, 0, 0, 0);
        elseif strcmp(childName, 'jointpos') || strcmp(childName, 'jointvel') || ...
                strcmp(childName, 'touch') || strcmp(childName, 'rangefinder') || ...
                strcmp(childName, 'tendonpos') || strcmp(childName, 'tendonvel') || ...
                strcmp(childName, 'actuatorpos') || strcmp(childName, 'actuatorvel') || ...
                strcmp(childName, 'actuatorfrc') || strcmp(childName, 'jointlimitpos') || ...
                strcmp(childName, 'jointlimitvel') || strcmp(childName, 'jointlimitfrc') || ...
                strcmp(childName, 'tendonlimitpos') || strcmp(childName, 'tendonlimitvel') || ...
                strcmp(childName, 'tendonlimitfrc')
            n = 1;
            nOutputs = nOutputs + n;
            getNames(child, 0, n, 0, 0);
        elseif strcmp(childName, 'accelerometer') || strcmp(childName, 'gyro') || ...
                strcmp(childName, 'velocimeter') || strcmp(childName, 'force') || ...
                strcmp(childName, 'torque') || strcmp(childName, 'magnetometer') || ...
                strcmp(childName, 'ballangvel') || strcmp(childName, 'framepos') || ...
                strcmp(childName, 'framexaxis') || strcmp(childName, 'frameyaxis') || ...
                strcmp(childName, 'framezaxis') || strcmp(childName, 'framelinvel') || ...
                strcmp(childName, 'frameangvel') || strcmp(childName, 'framelinacc') || ...
                strcmp(childName, 'frameangacc') || strcmp(childName, 'subtreecom') || ...
                strcmp(childName, 'subtreelinvel') || strcmp(childName, 'subtreeangmom')
            n = 3;
            nOutputs = nOutputs + n;
            getNames(child, 0, n, 0, 0);
        elseif strcmp(childName, 'ballquat') || strcmp(childName, 'framequat')
            n = 4;
            nOutputs = nOutputs + n;
            getNames(child, 0, n, 0, 0);
        end
        [nInputs, nOutputs, positions0, velocities0, inputNames, outputNames, positionNames, velocityNames, variableNames] = MuJoCoParseInternal(child, nInputs, nOutputs, positions0, velocities0, inputNames, outputNames, positionNames, velocityNames, variableNames);
    end
end

function writeNames(filePath, inputNames, outputNames, positionNames, velocityNames, variableNames)

    tab = "    ";

    data = '#pragma once\n\n';
    data = strcat(data, "constexpr unsigned int MJ_I_N = ", num2str(length(inputNames)), ';\n');
    data = strcat(data, "constexpr unsigned int MJ_O_N = ", num2str(length(outputNames)), ';\n');
    data = strcat(data, "constexpr unsigned int MJ_P_N = ", num2str(length(positionNames)), ';\n');
    data = strcat(data, "constexpr unsigned int MJ_V_N = ", num2str(length(velocityNames)), ';\n\n');
    
    data = strcat(data, 'struct MJ_I {\n');

    for index = 1:length(inputNames)
        data = strcat(data, tab, "static constexpr unsigned int ", upper(inputNames{index}), " = ", num2str(index - 1), ';\n');
    end

    data = strcat(data, '};\n\nstruct MJ_O {\n');
    
    for index = 1:length(outputNames)
        data = strcat(data, tab, "static constexpr unsigned int ", upper(outputNames{index}), " = ", num2str(index - 1), ';\n');
    end

    data = strcat(data, '};\n\nstruct MJ_P {\n');
    
    for index = 1:length(positionNames)
        data = strcat(data, tab, "static constexpr unsigned int ", upper(positionNames{index}), " = ", num2str(index - 1), ';\n');
    end

    data = strcat(data, '};\n\nstruct MJ_V {\n');
    
    for index = 1:length(velocityNames)
        data = strcat(data, tab, "static constexpr unsigned int ", upper(velocityNames{index}), " = ", num2str(index - 1), ';\n');
    end

    data = strcat(data, '};\n\nstruct MJ_M {\n');
    
    variableNames = table2cell(unique(cell2table(variableNames')));
    
    for index = 1:length(variableNames)
        if ~isvarname(variableNames{index})
            continue
        end
        try
            evaluated = evalin('base', variableNames{index});
        catch
            error('Template variable `%s` is invalid MATLAB variable.', variableNames{index});
        end
        if iscell(evaluated)
            error('Template variable `%s` cannot be a cell array.', variableNames{index});
        end
        if islogical(evaluated)
            if evaluated
                evaluated = 'true';
            else
                evaluated = 'false';
            end
            data = strcat(data, tab, "static constexpr bool ", variableNames{index}, " = ", evaluated, ';\n');
            continue;
        end
        if ~isvector(evaluated) || (~isreal(evaluated) && ~ischar(evaluated))
            error('Template variable `%s` is not a vector of real or characters.', variableNames{index});
        end
        evaluated = reshape(evaluated, 1, length(evaluated));
        if isreal(evaluated)
            if length(evaluated) == 1
                data = strcat(data, tab, "static constexpr double ", variableNames{index}, " = ", num2str(evaluated, '%.12f'), ';\n');
            else
                data = strcat(data, tab, "static constexpr double ", variableNames{index}, "[] = {\n");
                for subindex = 1:length(evaluated)
                    data = strcat(data, tab, tab, num2str(evaluated(subindex), '%.12f'));
                    if subindex ~= length(evaluated)
                        data = strcat(data, ',\n');
                    else
                        data = strcat(data, '\n');
                    end
                end
                data = strcat(data, tab, '};\n');
            end
            continue
        elseif ischar(evaluated)
             data = strcat(data, tab, "static constexpr char ", variableNames{index}, "[] = """, evaluated, '";\n');
             continue;
        end
    end
    
    data = strcat(data, '};');

    file = fopen(filePath, 'w+');
    fprintf(file, data);
    fclose(file);
end