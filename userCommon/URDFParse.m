function URDFParse(xmlPath)

    dom = xmlread(xmlPath);
    
    URDFParseInternal(dom);
    
    data = xmlwrite(dom);
    
    file = fopen('URDFModel.urdf', 'w+');
    fprintf(file, data);
    fclose(file);
end

function [] = URDFParseInternal(node)

    children = node.getChildNodes();    
    nChildren = children.getLength();
    
    supportedAttributes = {
        'xyz', 'rpy', 'length', 'radius', 'rgba', 'value', ...
        'ixx', 'iyy', 'izz', 'ixy', 'ixz', 'iyz', ...
        'damping', 'friction'
    };
    
    for index1 = 0:nChildren - 1
        child = children.item(index1);
        if child.getNodeType ~= 1 % Handle only tag nodes.
            continue
        end
        for index2 = 1:length(supportedAttributes)
            attribute = child.getAttribute(supportedAttributes{index2});
            if attribute.isEmpty()
                continue
            end
            attributeValue = convertCharsToStrings(attribute.toCharArray());
            attributeValue = strtrim(attributeValue{1, 1});
            splitted = split(attributeValue, ' ');
            for index3 = 1:length(splitted)
                if ~isvarname(splitted{index3})
                    continue
                end
                try
                    evaluated = evalin('base', splitted{index3});
                catch
                    error('Template variable `%s` is invalid MATLAB variable.', splitted{index3});
                end
                if islogical(evaluated)
                    if evaluated
                        evaluated = 'true';
                    else
                        evaluated = 'false';
                    end
                end
                if ~isvector(evaluated) || (~isreal(evaluated) && ~ischar(evaluated))
                    error('Template variable `%s` is not a vector of real numbers.', splitted{index3});
                end
                evaluated = reshape(evaluated, 1, length(evaluated));
                if isreal(evaluated)
                    splitted{index3} = num2str(evaluated, '%.12f ');
                elseif ischar(evaluated)
                    splitted{index3} = evaluated;
                end
            end
            child.setAttribute(supportedAttributes{index2}, strjoin(splitted));
        end
        URDFParseInternal(child);
    end
end