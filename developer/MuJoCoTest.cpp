#include "mujoco.h"

int finish(int code, mjModel *m = 0, mjData *d = 0)
{
	if (d) {
		mj_deleteData(d);
	}

	if (m) {
		mj_deleteModel(m);
	}

	mj_deactivate();

	return code;
}

int main(int argc, char **argv) {

	mj_activate("mjkey.txt");

	const int errorSize = 1000;
	char error[errorSize];

	mjModel *m = mj_loadXML(argv[1], 0, error, errorSize);
	if (!m) {
		return finish(1, m, 0);
	}

	mjData *d = mj_makeData(m);
	if (!d) {
		return finish(1, m, d);
	}

	return finish(0, m, d);
}